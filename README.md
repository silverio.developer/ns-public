# NearbyStores - Best Offers Around You !

Nearbystores is an intelligent and innovative offers search app within your zone including many smart tools and functionality to offer you the best experience in stores and events searching!

# Main Features:

- Get nearby stores : restaurants, hotels, gym clubs, coffees, pharmacies, sushi and more ...
- Get offers, Events near you
- Chat Realtime & Unlimited ( You can chat with your neighbors and the businesses owners without limit )
- Paymeny integration such as Paypal & Stripe


# Android app feature:

- Developed with Android Studio & Gradle
- Top quality, clean code created by experienced senior Android developer
- Localization Ready!
- Google Analytics
- Google Directions.
- Chat Realtime & Unlimited
- Support Multi Languages
- Currency management
- Push notification – firebase cloud messaging
- Custom offers
- Advanced PHP dashboard
- Permanent data storage (chat messages, stores and event informations) on a MYSQL database
- Support RTL Languages
- Google Analytics
- AdMob
- User Rating
- User Reviews
- Navigation drawer menu with categories
- Cool Mobile UI Interaction
- Material Design pattern
- Pull Down Bouncy UI.
- Share to Facebook
- Share to Twitter
- Call Integration
- SMS Integration
- User can add Favorites
- Advance Search
- About Us Panel Added
- Image Pinch and Zoom
- Well documented
- Participate to the event
- Upcoming events near you
- Parallax scrolling effect

# Web Admin Features

- Simple and Attractive Admin Panel.
- Display total stores events and customers created in your app
- Manage customers and their permission
- Manage Stores , Events , categories , Offers and more
- Secure all communications between the Web admin and android app with a crypto key
- Easily integrate new languages to your app
- Manage all the content created by your customers, edit the number of stores , offers, compagin … created by each one so quickly
- Use your phone where you are to manage your application and stay tuned to all your customer’s thanks to the responsive dashboard
- Stay tuned to your customers by answering their messages directly from dashboard


# Screenshot

![alt text](screenshot/1.png "screenshot")
![alt text](screenshot/2.png "screenshot")
![alt text](screenshot/3.png "screenshot")
![alt text](screenshot/4.png "screenshot")
![alt text](screenshot/5.png "screenshot")
![alt text](screenshot/6.png "screenshot")
![alt text](screenshot/7.png "screenshot")
![alt text](screenshot/8.png "screenshot")
![alt text](screenshot/9.png "screenshot")
![alt text](screenshot/10.png "screenshot")
![alt text](screenshot/11.png "screenshot")
![alt text](screenshot/12.png "screenshot")
![alt text](screenshot/13.png "screenshot")
![alt text](screenshot/14.png "screenshot")
![alt text](screenshot/15.png "screenshot")
![alt text](screenshot/16.png "screenshot")
![alt text](screenshot/17.png "screenshot")
![alt text](screenshot/18.png "screenshot")

